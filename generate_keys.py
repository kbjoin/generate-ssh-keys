import os
import random
import shutil
import sys
import string
import subprocess

if sys.argv.__contains__('-h'):
    print()
    print('-----Help file for script-----')
    print()
    print('syntax:')
    print('generate_keys.py <- runs script')
    print('generate_keys.py -h <- Prints this page')
    print('generate_keys.py -s <- Keeps generated keys after script exits')
    print('-----End of Help-----')
    sys.exit()

# ssh key accepted bit values
dsa_bit_values = ['1024']
ecdsa_bit_values = ['256', '384', '521']
rsa_bit_values = ['1024', '2048', '3072', '4096', '8192', '16384']
Ed25519_bit_values = ['256']  # all Ed25519 keys are 256

# password length generation parameters
min_pw_len = 10
max_pw_len = 100

dsa_results = {}
ecdsa_results = {}
rsa_results = {}
Ed25519_results = {}

temp_dir = '/tmp/keys'
if os.path.exists(temp_dir):
    shutil.rmtree(temp_dir)
os.makedirs(temp_dir)


# generate password based on min and max password length
def generate_password():
    pw_len = random.randint(min_pw_len, max_pw_len)
    pw = ""
    for i in range(pw_len):
        pw += random.choice(string.ascii_letters)
    return pw


# Generating DSA Keys
def generate_dsa_keys():
    dsa_dir = temp_dir + '/dsa'
    os.makedirs(dsa_dir)
    for value in dsa_bit_values:
        print('Generating DSA Keys')
        pw = ''
        subprocess.run(
            ['ssh-keygen', '-b', value, '-t', 'dsa', '-f', '{}/dsa-{}-nopass'.format(dsa_dir, value), '-m', 'rfc4716',
             '-N', str(pw)])
        pw = generate_password()
        subprocess.run(
            ['ssh-keygen', '-b', value, '-t', 'dsa', '-f', '{}/dsa-{}-pass'.format(dsa_dir, value), '-m', 'rfc4716',
             '-N',
             str(pw)])
    for file in os.listdir(dsa_dir):
        if file.endswith('.pub'):
            os.remove(dsa_dir + '/' + file)
    for value in dsa_bit_values:
        dsa_results[value] = []
        dsa_results[value].append('dsa-{}-pass'.format(value))
        key_pass_size = os.stat(dsa_dir + '/' + 'dsa-{}-pass'.format(value)).st_size
        dsa_results[value].append(key_pass_size)
        dsa_results[value].append('dsa-{}-nopass'.format(value))
        key_nopass_size = os.stat(dsa_dir + '/' + 'dsa-{}-nopass'.format(value)).st_size
        dsa_results[value].append(key_nopass_size)
        dsa_results[value].append(key_pass_size - key_nopass_size)
    return dsa_results


# Generating ecdsa Keys
def generate_ecdsa_keys():
    ecdsa_dir = temp_dir + '/ecdsa'
    os.makedirs(ecdsa_dir)
    for value in ecdsa_bit_values:
        print('Generating ECDSA Keys' + value)
        pw = ''
        subprocess.run(
            ['ssh-keygen', '-b', value, '-t', 'ecdsa', '-f', '{}/ecdsa-{}-nopass'.format(ecdsa_dir, value), '-m',
             'rfc4716',
             '-N', str(pw)])
        pw = generate_password()
        subprocess.run(
            ['ssh-keygen', '-b', value, '-t', 'ecdsa', '-f', '{}/ecdsa-{}-pass'.format(ecdsa_dir, value), '-m',
             'rfc4716', '-N',
             str(pw)])
    for file in os.listdir(ecdsa_dir):
        if file.endswith('.pub'):
            os.remove(ecdsa_dir + '/' + file)
    for value in ecdsa_bit_values:
        ecdsa_results[value] = []
        ecdsa_results[value].append('ecdsa-{}-pass'.format(value))
        key_pass_size = os.stat(ecdsa_dir + '/' + 'ecdsa-{}-pass'.format(value)).st_size
        ecdsa_results[value].append(key_pass_size)
        ecdsa_results[value].append('ecdsa-{}-nopass'.format(value))
        key_nopass_size = os.stat(ecdsa_dir + '/' + 'ecdsa-{}-nopass'.format(value)).st_size
        ecdsa_results[value].append(key_nopass_size)
        ecdsa_results[value].append(key_pass_size - key_nopass_size)
    return ecdsa_results


# Generating RSA Keys
def generate_rsa_keys():
    rsa_dir = temp_dir + '/rsa'
    os.makedirs(rsa_dir)
    for value in rsa_bit_values:
        print('Generating RSA Keys' + value)
        pw = ''
        subprocess.run(
            ['ssh-keygen', '-b', value, '-t', 'rsa', '-f', '{}/rsa-{}-nopass'.format(rsa_dir, value), '-m', 'rfc4716',
             '-N', str(pw)])
        pw = generate_password()
        subprocess.run(
            ['ssh-keygen', '-b', value, '-t', 'rsa', '-f', '{}/rsa-{}-pass'.format(rsa_dir, value), '-m', 'rfc4716',
             '-N',
             str(pw)])
    for file in os.listdir(rsa_dir):
        if file.endswith('.pub'):
            os.remove(rsa_dir + '/' + file)
    for value in rsa_bit_values:
        rsa_results[value] = []
        rsa_results[value].append('rsa-{}-pass'.format(value))
        key_pass_size = os.stat(rsa_dir + '/' + 'rsa-{}-pass'.format(value)).st_size
        rsa_results[value].append(key_pass_size)
        rsa_results[value].append('rsa-{}-nopass'.format(value))
        key_nopass_size = os.stat(rsa_dir + '/' + 'rsa-{}-nopass'.format(value)).st_size
        rsa_results[value].append(key_nopass_size)
        rsa_results[value].append(key_pass_size - key_nopass_size)
    return rsa_results


# Generating Ed25519 Keys
def generate_Ed25519_keys():
    Ed25519_dir = temp_dir + '/Ed25519'
    os.makedirs(Ed25519_dir)
    for value in Ed25519_bit_values:
        print('Generating Ed25519 Keys' + value)
        pw = ''
        subprocess.run(
            ['ssh-keygen', '-b', value, '-t', 'Ed25519', '-f', '{}/Ed25519-{}-nopass'.format(Ed25519_dir, value), '-m',
             'rfc4716',
             '-N', str(pw)])
        pw = generate_password()
        subprocess.run(
            ['ssh-keygen', '-b', value, '-t', 'Ed25519', '-f', '{}/Ed25519-{}-pass'.format(Ed25519_dir, value), '-m',
             'rfc4716', '-N',
             str(pw)])
    for file in os.listdir(Ed25519_dir):
        if file.endswith('.pub'):
            os.remove(Ed25519_dir + '/' + file)
    for value in Ed25519_bit_values:
        Ed25519_results[value] = []
        Ed25519_results[value].append('Ed25519-{}-pass'.format(value))
        key_pass_size = os.stat(Ed25519_dir + '/' + 'Ed25519-{}-pass'.format(value)).st_size
        Ed25519_results[value].append(key_pass_size)
        Ed25519_results[value].append('Ed25519-{}-nopass'.format(value))
        key_nopass_size = os.stat(Ed25519_dir + '/' + 'Ed25519-{}-nopass'.format(value)).st_size
        Ed25519_results[value].append(key_nopass_size)
        Ed25519_results[value].append(key_pass_size - key_nopass_size)
    return Ed25519_results


generate_dsa_keys()
generate_ecdsa_keys()
generate_rsa_keys()
generate_Ed25519_keys()

os.system('ssh -V 2>&1')
print('Printing results. All values are in bytes')
print('[encrypted-key, key size, unencrypted-key, key size, difference between encrypted/unencrypted keys]')
print(' ')
print('dsa results:')
for bit_length in dsa_results:
    print(dsa_results[bit_length])
print(' ')
print('ecdsa results:')
for bit_length in ecdsa_results:
    print(ecdsa_results[bit_length])
print(' ')
print('rsa results:')
for bit_length in rsa_results:
    print(rsa_results[bit_length])
print(' ')
print('Ed25519 results:')
for bit_length in Ed25519_results:
    print(Ed25519_results[bit_length])

if sys.argv.__contains__('-s'):
    sys.exit()
elif os.path.exists(temp_dir):
    shutil.rmtree(temp_dir)
