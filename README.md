# generate-ssh-keys

generate-ssh-keys.py is a tool for generating ssh keys of varying lengths.

## Installation

Download and run.  

Python 3 required.  Tested with v3.6.8 - v3.8.1

## Usage

Standard usage.  Creates keys in temp directory. Deletes the keys at the end of the script.
```bash
python3 generate-ssh-keys
```

Save mode.  Creates keys in temp directory.Does not delete the keys.
```bash
python3 generate-ssh-keys -s
```

Print Help
```bash
python3 generate-ssh-keys -h
```

## Output

Results are printed to console.  

```
OpenSSH_8.1p1 Debian-1, OpenSSL 1.1.1d  10 Sep 2019
Printing results. All values are in bytes
[encrypted-key, key size, unencrypted-key, key size, difference between encrypted/unencrypted keys
 
dsa results:
['dsa-1024-pass', 1413, 'dsa-1024-nopass', 1369, 44]
 
ecdsa results:
['ecdsa-256-pass', 557, 'ecdsa-256-nopass', 505, 52]
['ecdsa-384-pass', 667, 'ecdsa-384-nopass', 610, 57]
['ecdsa-521-pass', 780, 'ecdsa-521-nopass', 724, 56]
 
rsa results:
['rsa-1024-pass', 1077, 'rsa-1024-nopass', 1032, 45]
['rsa-2048-pass', 1856, 'rsa-2048-nopass', 1811, 45]
['rsa-3072-pass', 2635, 'rsa-3072-nopass', 2590, 45]
['rsa-4096-pass', 3414, 'rsa-4096-nopass', 3369, 45]
['rsa-8192-pass', 6529, 'rsa-8192-nopass', 6485, 44]
['rsa-16384-pass', 12761, 'rsa-16384-nopass', 12717, 44]
 
Ed25519 results:
['Ed25519-256-pass', 444, 'Ed25519-256-nopass', 399, 45]